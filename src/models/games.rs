use super::comments::Comment;
use crate::db::{ApiContext, MongoDocument};
use mongodb::bson::{doc, oid::ObjectId, Document};

pub struct Game {
    pub title: String,
    pub content: String,
    pub imgurl: String,
    pub id: String,
}
#[juniper::object(Context = ApiContext)]
#[graphql(description = "A game")]
impl Game {
    fn id(&self) -> &str {
        self.id.as_str()
    }
    fn title(&self) -> &str {
        self.title.as_str()
    }
    fn content(&self) -> &str {
        self.content.as_str()
    }
    fn imgurl(&self) -> &str {
        self.imgurl.as_str()
    }
    fn comments(&self, ctx: &ApiContext) -> Vec<Comment> {
        let collection = ctx.db.collection("comments");
        let comments = collection
            .find(Some(
                doc! {"articleid": ObjectId::with_string(self.id.as_str()).expect("invalid id")},
            ), None)
            .expect("fetching comments failed");

        let comments: Vec<Comment> = comments
            .into_iter()
            .map(|maybe_document| {
                let comment = maybe_document.unwrap();
                Comment::from_document(&comment)
            })
            .collect();

        comments
    }
}

impl Game {
    pub fn from_document(article: &Document) -> Self {
        Self {
            id: article.id(),
            title: article.field("title"),
            content: article.field("content"),
            imgurl: article.field("imgurl"),
        }
    }
}

#[derive(juniper::GraphQLInputObject)]
#[graphql(description = "A new game")]
pub struct NewGame {
    pub title: String,
    pub content: String,
    pub imgurl: String,
}
