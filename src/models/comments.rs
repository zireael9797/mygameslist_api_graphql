use super::users::User;
use crate::db::{ApiContext, MongoDocument};
use mongodb::bson::{doc, oid::ObjectId, Document};
#[derive(Clone)]
pub struct Comment {
    pub id: String,
    pub articleid: String,
    pub userid: String,
    pub comment: String,
}

#[juniper::object(Context = ApiContext)]
#[graphql(description = "A new comment")]
impl Comment {
    fn id(&self) -> &str {
        self.id.as_str()
    }
    fn articleid(&self) -> &str {
        self.articleid.as_str()
    }
    fn userid(&self) -> &str {
        self.userid.as_str()
    }
    fn comment(&self) -> &str {
        self.comment.as_str()
    }
    fn user(&self, ctx: &ApiContext) -> User {
        let collection = ctx.db.collection("users");
        let filter =
            doc! {"_id": ObjectId::with_string(self.userid.as_str()).expect("invalid user")};
        let maybe_document = collection
            .find_one(filter, None)
            .expect("fetching user failed");

        let user = maybe_document.unwrap();

        User::from_document(&user)
    }
}

impl Comment {
    pub fn from_document(comment: &Document) -> Self {
        Self {
            id: comment.id(),
            articleid: comment.id_field("articleid"),
            userid: comment.id_field("userid"),
            comment: comment.field("comment"),
        }
    }
}
