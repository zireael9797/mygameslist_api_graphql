use crate::db::MongoDocument;
use mongodb::bson::Document;

#[derive(Clone)]
pub struct User {
    pub id: String,
    pub username: String,
    pub email: String,
    pub hashedpass: String,
}

#[juniper::object]
#[graphql(description = "A user")]
impl User {
    fn id(&self) -> &str {
        self.id.as_str()
    }
    fn username(&self) -> &str {
        self.username.as_str()
    }
    fn email(&self) -> &str {
        self.email.as_str()
    }
}

impl User {
    pub fn from_document(user: &Document) -> Self {
        Self {
            id: user.id(),
            email: user.field("email"),
            username: user.field("username"),
            hashedpass: user.field("hashedpass"),
        }
    }
}
