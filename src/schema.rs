extern crate juniper;
use super::models::*;
use super::routes;
use crate::db::ApiContext;
use juniper::{FieldResult, RootNode};

pub struct QueryRoot;

#[juniper::object(Context = ApiContext)]
impl QueryRoot {
    fn articles(ctx: &ApiContext) -> FieldResult<Vec<Game>> {
        routes::games::articles(ctx)
    }

    fn article(id: String, ctx: &ApiContext) -> FieldResult<Game> {
        routes::games::article(id, ctx)
    }

    fn users(ctx: &ApiContext) -> FieldResult<Vec<User>> {
        routes::users::users(ctx)
    }

    fn user(ctx: &ApiContext) -> FieldResult<User> {
        routes::users::user(ctx)
    }
}

pub struct MutationRoot;

use super::routes::auth::Login;

#[juniper::object(Context = ApiContext)]
impl MutationRoot {
    fn login(email: String, password: String, ctx: &ApiContext) -> FieldResult<Login> {
        routes::auth::login(email, password, ctx)
    }

    fn register(
        email: String,
        password: String,
        username: String,
        ctx: &ApiContext,
    ) -> FieldResult<String> {
        routes::auth::register(email, password, username, ctx)
    }

    fn delete_user(ctx: &ApiContext) -> FieldResult<String> {
        routes::users::delete_user(ctx)
    }

    fn add_comment(comment: String, game_id: String, ctx: &ApiContext) -> FieldResult<String> {
        routes::comments::add_comment(comment, game_id, ctx)
    }

    fn delete_comment(comment_id: String, ctx: &ApiContext) -> FieldResult<String> {
        routes::comments::delete_comment(comment_id, ctx)
    }
}

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn create_schema() -> Schema {
    Schema::new(QueryRoot {}, MutationRoot {})
}
