extern crate juniper;
use crate::db::ApiContext;
use crate::models::*;
use juniper::{graphql_value, FieldError, FieldResult};
use mongodb::bson::{doc, oid::ObjectId};

pub fn users(ctx: &ApiContext) -> FieldResult<Vec<User>> {
    let collection = ctx.db.collection("users");

    if let Ok(users) = collection.find(None, None) {
        Ok(users
            .into_iter()
            .map(|maybe_document| {
                let user = maybe_document.unwrap();
                User::from_document(&user)
            })
            .collect())
    } else {
        Err(FieldError::new(
            "Fetching users failed",
            graphql_value!({ "error": "internal error" }),
        ))
    }
}
pub fn user(ctx: &ApiContext) -> FieldResult<User> {
    if let Some(token) = ctx.token.as_ref() {
        let collection = ctx.db.collection("users");
        if let Ok(id) = ObjectId::with_string(token) {
            let filter = doc! {"_id": &id};
            return match collection.find_one(filter, None) {
                Ok(Some(user)) => Ok(User::from_document(&user)),
                Ok(None) => Err(FieldError::new(
                    "Fetching user failed",
                    graphql_value!({ "error": "user does not exist" }),
                )),
                _ => Err(FieldError::new(
                    "Fetching user failed",
                    graphql_value!({ "error": "internal error" }),
                )),
            };
        } else {
            return Err(FieldError::new(
                "Fetching user failed",
                graphql_value!({ "error": "provided id is invalid" }),
            ));
        }
    }
    Err(FieldError::new(
        "Fetching user failed",
        graphql_value!({ "unauthorized": "user is not valid" }),
    ))
}

pub fn delete_user(ctx: &ApiContext) -> FieldResult<String> {
    if let Some(token) = ctx.token.as_ref() {
        if let Ok(id) = ObjectId::with_string(token) {
            return match ctx
                .db
                .collection("users")
                .find_one_and_delete(doc! {"_id": &id}, None)
            {
                Ok(Some(_)) => Ok("deleted".to_string()),
                Ok(None) => Err(FieldError::new(
                    "Delete failed",
                    graphql_value!({ "error": "user does not exist" }),
                )),
                _ => Err(FieldError::new(
                    "Delete failed",
                    graphql_value!({ "error": "internal error" }),
                )),
            };
        } else {
            return Err(FieldError::new(
                "Delete failed",
                graphql_value!({ "error": "provided id is invalid" }),
            ));
        }
    }
    Err(FieldError::new(
        "Delete failed",
        graphql_value!({ "error": "unauthorized" }),
    ))
}
