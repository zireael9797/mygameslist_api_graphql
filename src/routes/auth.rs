extern crate juniper;
use crate::db::ApiContext;
use crate::models::*;
use bcrypt::{hash, verify, DEFAULT_COST};
use hmac::{Hmac, NewMac};
use juniper::{graphql_value, FieldError, FieldResult};
use jwt::SignWithKey;
use mongodb::bson::doc;
use sha2::Sha256;
use std::collections::BTreeMap;

pub struct Login {
    token: Option<String>,
}
#[juniper::object(Context = ApiContext)]
impl Login {
    fn token(&self) -> Option<&String> {
        self.token.as_ref()
    }
}

pub fn login(email: String, password: String, ctx: &ApiContext) -> FieldResult<Login> {
    let collection = ctx.db.collection("users");
    let filter = doc! {"email": email};
    match collection.find_one(filter, None) {
        Ok(Some(document)) => {
            let user = User::from_document(&document);
            if let Ok(true) = verify(&password, &user.hashedpass) {
                let key: Hmac<Sha256> = Hmac::new_varkey(b"some-secret").unwrap();
                let mut claims = BTreeMap::new();
                claims.insert("sub", user.id);
                let token = claims.sign_with_key(&key).ok();
                return Ok(Login { token });
            } else {
                return Err(FieldError::new(
                    "Login failed",
                    graphql_value!({ "failure": "user not found" }),
                ));
            }
        }
        Ok(None) => {
            return Err(FieldError::new(
                "Login failed",
                graphql_value!({ "failure": "user not found" }),
            ));
        }
        _ => {}
    }
    Err(FieldError::new(
        "Login failed",
        graphql_value!({ "failure": "login was unsuccessful due to internal error" }),
    ))
}

pub fn register(
    email: String,
    password: String,
    username: String,
    ctx: &ApiContext,
) -> FieldResult<String> {
    let collection = ctx.db.collection("users");
    let email_filter = doc! {"email": &email};
    let username_filter = doc! {"username": &username};

    match (
        collection.find_one(email_filter, None),
        collection.find_one(username_filter, None),
        hash(&password, DEFAULT_COST),
    ) {
        (Ok(None), Ok(None), Ok(hashedpass)) => {
            if ctx
                .db
                .collection("users")
                .insert_one(
                    doc! {"email": &email, "hashedpass": &hashedpass, "username": &username},
                    None,
                )
                .is_ok()
            {
                return Ok(username);
            }
        }
        (Ok(Some(_)), Ok(Some(_)), _) => {
            return Err(FieldError::new(
                "Signup failed",
                graphql_value!({ "failure": "username or email already in use" }),
            ));
        }
        _ => {}
    }
    Err(FieldError::new(
        "Signup failed",
        graphql_value!({ "failure": "signup was unsuccessful due to internal error" }),
    ))
}
