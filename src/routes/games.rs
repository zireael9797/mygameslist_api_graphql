extern crate juniper;
use crate::db::ApiContext;
use crate::models::*;
use juniper::{graphql_value, FieldError, FieldResult};
use mongodb::bson::{doc, oid::ObjectId};

pub fn articles(ctx: &ApiContext) -> FieldResult<Vec<Game>> {
    let collection = ctx.db.collection("articles");

    collection
        .find(None, None)
        .map(|articles| {
            articles
                .into_iter()
                .map(|maybe_document| {
                    let article = maybe_document.unwrap();
                    Game::from_document(&article)
                })
                .collect()
        })
        .map_err(|_| {
            FieldError::new(
                "Fetching users failed",
                graphql_value!({ "error": "internal error" }),
            )
        })
}

pub fn article(id: String, ctx: &ApiContext) -> FieldResult<Game> {
    let collection = ctx.db.collection("articles");
    if let Ok(id) = ObjectId::with_string(id.as_str()) {
        let filter = doc! {"_id": &id};
        return match collection.find_one(filter, None) {
            Ok(Some(game)) => Ok(Game::from_document(&game)),
            Ok(None) => Err(FieldError::new(
                "Fetching game failed",
                graphql_value!({ "error": "game does not exist" }),
            )),
            _ => Err(FieldError::new(
                "Fetching game failed",
                graphql_value!({ "error": "internal error" }),
            )),
        };
    }
    Err(FieldError::new(
        "Fetching game failed",
        graphql_value!({ "error": "provided id is invalid" }),
    ))
}
