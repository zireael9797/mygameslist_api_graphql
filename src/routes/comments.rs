extern crate juniper;
use crate::db::ApiContext;
use juniper::{graphql_value, FieldError, FieldResult};
use mongodb::bson::{doc, oid::ObjectId};

pub fn add_comment(comment: String, game_id: String, ctx: &ApiContext) -> FieldResult<String> {
    if let Some(token) = ctx.token.as_ref() {
        if let (Ok(userid), Ok(gameid)) = (
            ObjectId::with_string(token),
            ObjectId::with_string(game_id.as_str()),
        ) {
            if let (Ok(Some(_)), Ok(Some(_))) = (
                ctx.db
                    .collection("users")
                    .find_one(doc! {"_id": &userid}, None),
                ctx.db
                    .collection("articles")
                    .find_one(doc! {"_id": &gameid}, None),
            ) {
                if let Ok(None) = ctx
                    .db
                    .collection("comments")
                    .find_one(doc! {"userid": &userid, "gameid": &gameid}, None)
                {
                    if ctx
                        .db
                        .collection("comments")
                        .insert_one(
                            doc! {"userid": &userid, "articleid": &gameid, "comment": &comment},
                            None,
                        )
                        .is_ok()
                    {
                        return Ok(comment);
                    }
                }
            }
        }
    }
    Err(FieldError::new(
        "Comment failed",
        graphql_value!({ "unauthorized": "user or game is not valid" }),
    ))
}

pub fn delete_comment(comment_id: String, ctx: &ApiContext) -> FieldResult<String> {
    if let Some(token) = ctx.token.as_ref() {
        if let (Ok(userid), Ok(commentid)) = (
            ObjectId::with_string(token),
            ObjectId::with_string(comment_id.as_str()),
        ) {
            if let Ok(Some(_)) = ctx
                .db
                .collection("comments")
                .find_one_and_delete(doc! {"userid": &userid, "_id": &commentid}, None)
            {
                return Ok("deleted".to_string());
            }
        }
    }
    Err(FieldError::new(
        "Comment failed",
        graphql_value!({ "unauthorized": "comment doesn't exist or unauthorized" }),
    ))
}
