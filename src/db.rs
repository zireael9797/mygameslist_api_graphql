use mongodb::{
    bson::{Bson, Document},
    sync::Client,
    sync::Database,
};
use std::env;

#[derive(Clone)]
pub struct ApiContext {
    pub db: &'static Database,
    pub token: Option<String>,
}
impl juniper::Context for ApiContext {}

pub fn connect() -> Database {
    let connection_string = env::var("MONGO_URL").expect("Mongo Connection not found");
    let client = Client::with_uri_str(connection_string.as_str()).expect("Connection failed");
    client.database("wikiDB")
}

impl MongoDocument for Document {
    fn field(&self, field: &str) -> String {
        self.get(field)
            .and_then(Bson::as_str)
            .expect(field)
            .to_owned()
    }
    fn id_field(&self, field: &str) -> String {
        self.get(field)
            .and_then(Bson::as_object_id)
            .expect("_id")
            .to_string()
    }
    fn id(&self) -> String {
        self.get("_id")
            .and_then(Bson::as_object_id)
            .expect("_id")
            .to_string()
    }
}

pub trait MongoDocument {
    fn field(&self, field: &str) -> String;
    fn id_field(&self, field: &str) -> String;
    fn id(&self) -> String;
}
