//! Actix web juniper example
//!
//! A simple example integrating juniper in actix-web
use dotenv::dotenv;
use std::env;
use std::io;
use std::sync::Arc;

use actix_web::{middleware, web, App, Error, HttpResponse, HttpServer};
use juniper::http::graphiql::graphiql_source;
use juniper::http::GraphQLRequest;
use mongodb::sync::Database;
use once_cell::sync::Lazy;

mod db;
mod models;
mod routes;
mod schema;

use db::ApiContext;
use schema::{create_schema, Schema};

static CLIENT: Lazy<Database> = Lazy::new(db::connect);

async fn graphiql() -> HttpResponse {
    let port = env::var("PORT").unwrap_or_else(|_| "8080".to_owned());
    let html = graphiql_source(&format!("http://127.0.0.1:{}/graphql", port));
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

use hmac::{Hmac, NewMac};
use jwt::VerifyWithKey;
use sha2::Sha256;
use std::collections::BTreeMap;

async fn graphql(
    st: web::Data<Arc<Schema>>,
    data: web::Json<GraphQLRequest>,
    request: web::HttpRequest,
) -> Result<HttpResponse, Error> {
    let token = request
        .headers()
        .get("token")
        .map(|token| token.to_str().ok().map(|token| token.to_string()))
        .unwrap_or(None);

    let key: Hmac<Sha256> = Hmac::new_varkey(b"some-secret").unwrap();

    let token = match token {
        Some(token) => {
            println!("token found");
            let claims: Result<BTreeMap<String, String>, jwt::Error> =
                token.as_str().verify_with_key(&key);
            match claims {
                Ok(claims) => match claims.get("sub") {
                    Some(id) => Some(id.to_owned()),
                    None => None,
                },
                Err(_) => None,
            }
        }
        None => None,
    };

    let res = data.execute(
        &st,
        &ApiContext {
            db: &*CLIENT,
            token,
        },
    );
    let result = serde_json::to_string(&res)?;
    Ok(HttpResponse::Ok()
        .content_type("application/json")
        .body(result))
}

#[actix_rt::main]
async fn main() -> io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    dotenv().ok();
    let port = env::var("PORT").unwrap_or_else(|_| "8080".to_owned());
    // Create Juniper schema
    let schema = std::sync::Arc::new(create_schema());

    // Start http server
    HttpServer::new(move || {
        App::new()
            .data(schema.clone())
            .wrap(middleware::Logger::default())
            .service(web::resource("/graphql").route(web::post().to(graphql)))
            .service(web::resource("/graphiql").route(web::get().to(graphiql)))
    })
    .bind(&format!("127.0.0.1:{}", port))?
    .run()
    .await
}
